<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class Counterparty extends Model
{
  protected $fillable = [
       'company_id',
       'type',
       'itn',
       'iec',
       'psrn',
       'name_1c',
       'doc_name',
       'information',
       'director_name',
       'director_position',
       'mail',
       'full_name',
       'short_name',
       'sertificat_number',
       'document_id',
       'date',
       'date_registration'

   ];
  public static $validation_message = [
        'required' => ':attribute is required',
        'email' => ':attribute is note email',
        'unique' => ':attribute already exist',
    ];
  public static function add($data)
    {
      try{
          $v = Validator::make($data, [
              'itn' => 'required',
              'name_1c' => 'required',
              'mail' => 'email'
          ],self::$validation_message);

          if ($v->fails()) {
              $status = Response::HTTP_BAD_REQUEST;
              $response = [
                  'status' => $status,
                  'message' => $v->errors(),
              ];
          }
          Counterparty::create($data);
          $status = Response::HTTP_OK;
          $response = [
              'status' => $status,
              'message' => 'Counterparty has been added successfully',
          ];
          return $response;
      }catch (\Exception $e){
          $status = Response::HTTP_BAD_REQUEST;
          $response = [
              'status' => $status,
              'message' => $e->getMessage(),
          ];
      }
      return $response;
    }

  
  public static function get($data)
    {
        try{
            $where = [];
            foreach ($data as $key => $value) {
		if($key!='target'){
		   $where[] = [$key,'=',$value];
            	}
	    }

            $counterparty = Counterparty::where($where)->get();
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'data' => $counterparty,
            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Data not found',
            ];
        }catch (\Exception $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }
        return $response;
    }

  public static function destroy($data)
    {
        try {
            $counterparty = Counterparty::findOrFail($data->id);
            $counterparty->delete();
            $status = Response::HTTP_OK;
            $response = [
                'status' => $status,
                'message' => 'Counterparty has been deleted successfully'
            ];
        }catch (ModelNotFoundException $e){
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Data not found',
            ];
        } catch (\Exception $e) {
            $status = Response::HTTP_BAD_REQUEST;
            $response = [
                'status' => $status,
                'message' => 'Oops, something went wrong',
            ];
        }
        return $response;
    }

}
