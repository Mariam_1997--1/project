<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Counterparty;

class CounterpartiesController extends Controller
{
    public function getCounterparties(Request $request)
  {
      $data = $request->all();
      $response = Counterparty::get($data);
      return response()->json($response, $response['status']);
  }
  public function addCounterparty(Request $request)
  {
      $data = $request->all();
      $response = Counterparty::add($data);
      return response()->json($response, $response['status']);

  }
  public function destroy(Request $request)
  {
      $response = Counterparty::destroy($request);
      return response()->json($response, $response['status']);

  }
}
