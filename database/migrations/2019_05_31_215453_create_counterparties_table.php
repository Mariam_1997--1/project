<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCounterpartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counterparties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('company_id');
            $table->tinyInteger('type');
            $table->string('itn')->nullable();
            $table->string('iec')->nullable();
            $table->string('psrn')->nullable();
            $table->string('name_1c');
            $table->string('doc_name')->nullable();
            $table->string('address')->nullable();
            $table->text('information')->nullable();
            $table->string('director_name')->nullable();
            $table->string('director_position')->nullable();
            $table->string('mail')->nullable();
            $table->string('full_name')->nullable();
            $table->string('short_name')->nullable();
            $table->string('sertificat_number')->nullable();
            $table->string('document_id')->nullable();
            $table->date('date')->nullable();
            $table->date('date_registration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counterparties');
    }
}
